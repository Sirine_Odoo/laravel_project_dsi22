<?php

use App\Models\Administration;
use App\Models\Medecin;
use App\Models\Patient;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
            factory(Medecin::class, 10)->create();
            factory(Patient::class, 20)->create();
            factory(User::class, 20)->create();
            factory(Administration::class, 20)->create();
            
    }
}
