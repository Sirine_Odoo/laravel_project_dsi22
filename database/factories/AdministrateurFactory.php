<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Medecin;
use App\Models\Patient;
use Faker\Generator as Faker;

$factory->define(App\Models\Administration::class, function (Faker $faker) {
   $medecin=Medecin::All()->pluck('id')->toArray();
   $patient=Patient::All()->pluck('id')->toArray();


    return [
        'firstname' => $faker->name,
        'lastname' => $faker->name,
        'address' =>$faker->address,
        'phone' => $faker->phoneNumber,
        'email' =>$faker->email,
       'medecin_id' =>$faker->randomElement($medecin),
        'patient_id' =>$faker->randomElement($patient),
        'created_at' => now(),
        'updated_at' => now(),
    ];
});