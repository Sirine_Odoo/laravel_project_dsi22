<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\Medecin::class, function (Faker $faker) {
    return [
        'firstname' => $faker->name,
        'lastname' => $faker->name,
        'address' =>$faker->address,
        'phone' => $faker->phoneNumber,
        'created_at' => now(),
        'updated_at' => now(),
    ];
});
