<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\Patient::class, function (Faker $faker) {
    return [
        'firstname' => $faker->name,
        'lastname' => $faker->name,
        'email' => $faker->safeEmail,
        'phone' => $faker->phoneNumber,
        'address' =>$faker->address,
        'created_at' => now(),
        'updated_at' => now(),
        'medecin_id' => function(){
            return App\Models\Medecin::all()->random();
        }
    ];
});
