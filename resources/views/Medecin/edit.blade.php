    @extends('layouts.master')

@section('title')
    Medecin-Registered | Hospital Management
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3>Edit Role For Registered Medecin</h3>
                </div>
                <div class="card-body">
                <div class="row">
                        <div class="col-md-6">
                        <form action="/medecin-update/{{ $medecins->id }}" method="POST">
                        {{csrf_field()}}
                        {{method_field('PUT')}}

                        <div class="form-group">
                            <label>Nom</label>
                            <input type="texte" name="username" value="{{ $medecins->firstname }}" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>Lastname</label>
                            <input type="texte" name="lastname" value="{{ $medecins->lastname }}" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>Phone</label>
                            <input type="texte" name="phone" value="{{ $medecins->phone }}" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>Address</label>
                            <input type="texte" name="address" value="{{ $medecins->address }}" class="form-control">
                        </div>
                                        

                     <div class="modal-footer">
                     <a href="{{ url('Medecin') }}" class="btn btn-secondary">BACK</a>                     
                    <button type="submit" class="btn btn-success">Update</button>
                     
                    </div>
            </form>
                </div>
            </div>
        </div>
       
@endsection


@section('scripts')
@endsection
     
