@extends('layouts.master')

@section('title')
    Medecins List | Hospital Management
@endsection

@section('content')





<div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Our Medecins
                </h4>
                @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
              </div>
            

              <div class="card-body">
                <div class="table-responsive">
                <table class="table">
                    <thead class=" text-primary">
                        <th>ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Address</th>
                        <th>Phone</th>
                        <th>Edit</th>
                        <th>Delete</th>
                      
                                       
                    </thead>
                        <tbody>
                           @foreach ($medecins as $row)

                            <tr>
                            <td>{{ $row->id }}</td>
                            <td>{{ $row->firstname }}</td>
                            <td>{{ $row->lastname }}</td>
                            <td>{{ $row->address }}</td>
                            <td>{{ $row->phone }}</td>     
                            
                            <td>
                              <a href="/medecin-edit/{{ $row->id }}" class="btn btn-success">EDIT</a>
                          </td>
                         
                          <td>
                          <form method="POST" action="{{ url('/medecin-delete/{id}/' .$row->id) }}">
                          {{ csrf_field() }}
                          {{ method_field('DELETE')}}   
                          <button type="submit" class="btn btn-danger">DELETE</button>  
                          </form>                 
                          </td>  
                            
                        </tr>
                        @endforeach
                     
                    </tbody>                    
                  
                  </table>
                </div>
              </div>
            </div>
          </div>
        
          

          
@endsection


@section('scripts')

@endsection
