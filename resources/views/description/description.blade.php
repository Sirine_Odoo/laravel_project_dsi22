@extends('layouts.master')

@section('title')
    Description| Hospital Management
@endsection

@section('content')
<div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Our Hospital
                </h4>
                @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
              
              <div class="card-body">
                <div class="table-responsive">
                    <h4>
                    Hospital are the essential part of our lives, 
                    providing best medical facilities to people suffering
                    from various ailments, which may be due to change in climatic conditions, 
                    increased work-load, emotional trauma stress etc. It is necessary for 
                    the hospitals to keep track of its day-to-day activities & records of its patients,
                    doctors, nurses, ward boys and other staff personals that keep the hospital running
                    smoothly & successfully .
                    </h4>
                    <div class="hospital-img">
                           <img src="{{ asset('assethome/images/hosp.jpeg')}}" alt="" style="width: 800px; height: 400px;" class="img-thumbnail">    
                    </div>  
                                      
                 
                </div>                
                </div>
          
                <div class="content">
                    <div class="title m-b-md">
                    Our Staff
                </div>   

                <br>
             
                <figure class="figure">
                  <img src="{{ asset('assethome/images/doctor1.jpg')}}" style="width: 400px; height: 250px;" class="figure-img img-fluid rounded"  alt="A generic square placeholder image with rounded corners in a figure.">
                  <figcaption class="figure-caption text-right">Geraldine Klocko, doctor of otorhinolaryngologists 
                    (also known as otolaryngologists or ear,
                     nose and throat or ENT Surgeons) are surgical specialists who diagnose,
                      evaluate and manage a wide range of 
                     diseases of the head and neck, including the ear, nose and throat regions.</figcaption>
                </figure>
                <br>

                <figure class="figure">
                  <img src="{{ asset('assets/img/ali.jpg')}}" style="width: 400px; height: 250px;" class="figure-img img-fluid rounded"  alt="A generic square placeholder image with rounded corners in a figure.">
                  <figcaption class="figure-caption text-right">Jennyfer Hyatt, obstetrics and gynaecology is concerned with the care of pregnant woman,
                     her unborn child and the management of diseases specific to women.
                     The specialty combines medicine and surgery.</figcaption>
                </figure>
                <br>

                <figure class="figure">
                  <img src="{{ asset('assets/img/farman.jpg')}}" style="width: 400px; height: 250px;" class="figure-img img-fluid rounded"  alt="A generic square placeholder image with rounded corners in a figure.">
                  <figcaption class="figure-caption text-right">This is our doctor Velda Ankunding</figcaption>
                </figure>
                <br>

                <figure class="figure">
                  <img src="{{ asset('assets/img/tanjo.jpeg')}}" style="width: 400px; height: 250px;" class="figure-img img-fluid rounded"  alt="A generic square placeholder image with rounded corners in a figure.">
                  <figcaption class="figure-caption text-right">This is our doctor Melany Volkman</figcaption>
                </figure>
                <br>

                    

           
                         
            </div>

  


@endsection


@section('scripts')



@endsection