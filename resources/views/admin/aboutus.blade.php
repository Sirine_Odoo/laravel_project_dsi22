@extends('layouts.master')

@section('title')
    About Us | Gestion Hopitalière
@endsection

@section('content')
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add About Us</h5>
       
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
              
        </div>
          <form action="/save-aboutus" method="POST">
          {{ csrf_field()}}
          <div class="modal-body">             
             <div class="form-group">
                <label for="title-name" class="col-form-label">Title:</label>
                <input type="text" name="title" class="form-control" id="title-name">
              </div>
              
              <div class="form-group">
                <label for="sub-title-name" class="col-form-label">Sub-Title:</label>
                <input type="text" name="subtitle" class="form-control" id="sub-title-name">
              </div>

              <div class="form-group">
                <label for="description-text" class="col-form-label">Description:</label>
                <textarea name="description"  class="form-control" id="message-text"></textarea>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>    
          </div>
          </form>

    </div>
  </div>
</div>

{{-- Delete - Modal --}}

<div class="modal fade" id="deletemodalpop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form method="POST" id="delete_modal_Form">
       {{ csrf_field()}}
       {{ method_field('DELETE')}}           

      <div class="modal-body">
        <input type="hidden" id="delete_aboutus_id">
          <h4>Are yous sure. ? you want to deletethis Data  ?</h4>
        </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Yes !! Delete it</button>
      </div>
      </form>
    </div>
  </div>
</div>

{{-- End- Delete Modal--}}

<div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"> About Us
               <button type="button" class="btn btn-primary float-right" data-toggle="modal"
                data-target="#exampleModal">ADD</button>      
            </h4>
            @if (session('status'))
                 <div class="alert alert-success" role="alert">
                      {{ session('status') }}
                  </div>
              @endif
              </div>
             <style>
                .w-10p {
                  width: 10% !important;
                }
              </style>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table">
                    <thead class="text-primary">
                      <th class="w-10p">ID</th>
                      <th class="w-10p">Title</th>
                      <th class="w-10p">Sub-Title</th>
                      <th class="w-10p">Description</th>      
                      <th class="w-10p">Edit</th>
                      <th class="w-10p">Delete</th>                
                    </thead>
                    <tbody>
                      @foreach ($aboutus as $data)            
                      <tr>
                          <td>{{ $data->id}}</td>
                          <td>{{ $data->title}}</td>
                          <td>{{ $data->subtitle}}</td>
                          <td>
                            <div style="height:80px; overflow:hidden;">{{ $data->description}}</div></td>
                          
                            <td>
                              <a href="{{ url('about-us/'. $data->id)}}" class="btn btn-success">EDIT</a>
                          </td>

                          <td>
                      <!-- <a href="javascript:void(0)" class="btn btn-danger deletebtn"
                          data-toggle="modal" data-target="#deletemodalpop">DELETE</a> -->
                          <form method="POST" action="{{ url('about-us-delete/' .$data->id) }}">
                          {{ csrf_field()}}
                          {{ method_field('DELETE')}}  
                          <button type="submit" class="btn btn-danger deletebtn">DELETE</button>  
                          </form>                 
                          </td>   
                        </tr>
                        @endforeach
                    </tbody>                  
                  </table>
                </div>
              </div>
            </div>
          </div>
         </div>
        
@endsection


@section('scripts')
  <script>
    $(document).ready(function ()
    {
      $('#datatable').DataTable();

      $('selector').on('click' , '.deletebtn' , function()
      {
          $tr = $(this).closest('tr');

          var data=$tr.children("td").map(function (){
            
            return $(this).text();
            
          }).get();

          //console.log(data);

          $('#delete_aboutus_id').val(data[0]);
          $('#delete_modal_Form').attr('action' , '/about-us-delete'+data[0]);
          $('#deletemodalpop').modal('show');

      });
    });
      </script>

@endsection