<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::resource('/Medecin', 'MedecinController');

Route::group(['middledware' => ['auth' , 'admin']], function(){


    Route::get('/dashboard', function () {
        return view('admin.dashboard');
});

Route::get('/role-register', 'Admin\DashboardController@registered');

   Route::get('/role-edit/{id}', 'Admin\DashboardController@registerededit');
   Route::put('/role-register-update/{id}','Admin\DashboardController@registerupdate');
   Route::delete('/role-delete/{id}','Admin\DashboardController@registerdelete');

    Route::get('/abouts', 'Admin\AboutusController@index');
    Route::post('/save-aboutus', 'Admin\AboutusController@store');
    Route::get('/about-us/{id}', 'Admin\AboutusController@edit');    
    Route::put('/aboutus-update/{id}', 'Admin\AboutusController@update');
    Route::delete('/about-us-delete/{id}', 'Admin\AboutusController@delete');



    Route::get('/medecin-edit/{id}', 'MedecinController@medecinedit');
//   Route::put('/medecin-update/{id}','Medecin\MedecinController@medecinupdate');
   Route::put('/medecin-update/{id}','MedecinController@medecinupdate');
    Route::delete('/medecin-delete/{id}','MedecinController@medecindelete'); //ce route va te cuaser un pblm aussi

   Route::get('/description', 'Admin\DescriptionController@index');

});
