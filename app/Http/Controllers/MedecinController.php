<?php

namespace App\Http\Controllers;

use App\Models\Medecin;
use Illuminate\Http\Request;

class MedecinController extends Controller
{
    
    public function index ()
    {   
        $medecins = Medecin::all();
        return view('Medecin.index')->with('medecins',$medecins); 

    }

    public function medecinedit(Request $request, $id)
    {
       $medecins = Medecin::findOrFail($id) ; 
       return view('Medecin.edit')->with('medecins',$medecins); 

    }

    public function medecinupdate(Request $request,$id)
    {
           $medecins = Medecin::find($id) ;
           $medecins->phone = $request->input('phone');
           $medecins->address = $request->input('address');
           $medecins->update();
            
            return redirect('/Medecin')->with('status','Your Data is Updated');        
    
    }

    public function medecindelete($id)
    {
        $medecins = Medecin::findOrFail($id) ;
        $medecins->delete();
        
        return redirect('/medecin')->with('status','Your Data is Deleted');  
    
    }


}



