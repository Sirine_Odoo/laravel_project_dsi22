<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Administration extends Model
{
    protected $fillable = ['firstname','lastname','address','phone'];

    public function patients()
    {
        return $this->belongsTo('App\Patient');

    }
    public function medecin()
    {
        return $this->belongsTo('App\Medecin');

    }
}
