<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Medecin extends Model
{
    protected $fillable = ['firstname','lastname','address','phone'];

    public function patients()
    {
        return $this->hasMany('App\Medecin');
    }
}
