<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $fillable = ['firstname','lastname','address','phone','email'];
    
    public function medecin(){

        return $this->belongsTo('App\Medecin');
    }
}
